const path = require('path')

function addStyleResource (rule) {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [
        path.resolve(__dirname, './src/assets/style/*.scss'),
      ],
    })
}

module.exports = {
  siteName: 'GZZ WP Test',
  siteDescription: 'Blah blah...',
  templates: {
    WordPressCategoryOriginal: '/orig/category/:slug', // orginal Gridsome devs
    WordPressPostOriginal: '/orig/:year/:month/:day/:slug', // orginal Gridsome devs
    WordPressPostTagOriginal: '/orig/tag/:slug', // orginal Gridsome devs
    WordPressCategory: '/peha/category/:slug', // polka
    WordPressPost: '/peha/:year/:month/:day/:slug', // polka
    WordPressPostTag: '/peha/tag/:slug', // polka
    WordPressAuthor: '/peha/author/:slug', //polka
    WordPressPage: '/page/:slug'
  },
  plugins: [
    {
      use: '@gridsome/source-wordpress',
      options: {
        baseUrl: 'https://www.grizzlink.cz/', // required - Replace me with your Wordpress URL
        apiBase: 'wp-json',
        typeName: 'WordPress', // GraphQL schema name (Optional)
        perPage: 100, // How many posts to load from server per request (Optional)
        concurrent: 10, // How many requests to run simultaneously (Optional)
        routes: {
          post: '/:year/:month/:day/:slug', //adds route for "post" post type (Optional)
          post_tag: '/tag/:slug', // adds route for "post_tag" post type (Optional)
          category: '/category/:slug', // adds route for "category" post type
          author: '/author/:slug',
          page: '/page/:slug'
        },
        createPages: {
          approach: 'include', // include or exclude, default is include
          list: ['sample-page', 'co-je-grizzlink', 'co-umi-agentura-grizzlink', 'nase-prace', 'blog-o-marketingu-komunikaci-reklame', 'jak-nas-vystopujete', 'kariera' ] // an array of page slugs to include or exclude, ex. ['about', 'our-team'], default is an empty array
        }
      }
    }
  ],
  chainWebpack (config) {
    // Load variables for all vue-files
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal']

    types.forEach(type => {
      addStyleResource(config.module.rule('scss').oneOf(type))
    })
  }
}
